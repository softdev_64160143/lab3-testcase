/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.purin.lab3;

/**
 *
 * @author tin
 */
class OXProgram {

    static boolean checkwin(String[][] table, String yourTurn) {
        if (checkRow(table, yourTurn)) {
            return true;
        }
        return false;
    }

    private static boolean checkRow(String[][] table, String yourTurn) {
        for (int row = 0; row<3; row++) {
            if (checkRow(table,yourTurn,row)) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkRow(String[][] table, String yourTurn, int row) {
        return table[row][0].equals(yourTurn) && table[row][1].equals(yourTurn);
    }
    
}
