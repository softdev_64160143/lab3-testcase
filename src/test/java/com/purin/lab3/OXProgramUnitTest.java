/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.purin.lab3;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author tin
 */
public class OXProgramUnitTest {
    
    public OXProgramUnitTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    } 

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void testCheckWinNoPlayBy_O_Output_false(){
        String[][] table = {{"-","-","-"},{"-","-","-"},{"-","-","-"}};
        String yourTurn = "O";
        assertEquals(false, OXProgram.checkwin (table,yourTurn));
                }
     @Test
    public void testCheckWin1By_X_output_true(){
        String[][] table = {{"-","-","-"},{"O","O","O"},{"-","-","-"}};
        String yourTurn = "O";
        assertEquals(true, OXProgram.checkwin (table,yourTurn));
                }
        // public void hello() {}
      @Test
    public void testCheckWinRow1By_X_output_true(){
        String[][] table = {{"X","X","X"},{"-","O","O"},{"-","-","-"}};
        String yourTurn = "X";
        assertEquals(true, OXProgram.checkwin (table,yourTurn));
                }
    
         @Test
    public void testCheckWin1_X_output_true(){
        String[][] table = {{"X","-","O"},{"X","O","-"},{"X","-","-"}};
        String yourTurn = "X";
        assertEquals(false, OXProgram.checkwin (table,yourTurn));
                }
    
           @Test
    public void testCheckWin1_O_output_true(){
        String[][] table = {{"X","O","X"},{"-","O","X"},{"-","O","-"}};
        String yourTurn = "O";
        assertEquals(false, OXProgram.checkwin (table,yourTurn));
                }
    
              @Test
    public void testCheckWin2_X_output_true(){
        String[][] table = {{"O","-","X"},{"-","O","X"},{"-","O","X"}};
        String yourTurn = "X";
        assertEquals(false, OXProgram.checkwin (table,yourTurn));
                }
    
             @Test
    public void testCheckWin2_O_output_true(){
        String[][] table = {{"O","O","O"},{"-","X","X"},{"-","-","X"}};
        String yourTurn = "O";
        assertEquals(true, OXProgram.checkwin (table,yourTurn));
                }
    
      @Test
    public void testCheckWin3_X_output_true(){
        String[][] table = {{"O","O","-"},{"X","X","X"},{"-","-","-"}};
        String yourTurn = "X";
        assertEquals(true, OXProgram.checkwin (table,yourTurn));
                }
    
        @Test
    public void testCheckWin3_O_output_true(){
        String[][] table = {{"X","-","-"},{"-","-","X"},{"O","O","O"}};
        String yourTurn = "O";
        assertEquals(true, OXProgram.checkwin (table,yourTurn));
                }
    
        @Test
    public void testCheckWin4_X_output_true(){
        String[][] table = {{"X","-","O"},{"-","X","O"},{"-","-","X"}};
        String yourTurn = "X";
        assertEquals(false, OXProgram.checkwin (table,yourTurn));
                }
    
       @Test
    public void testCheckWin4_O_output_true(){
        String[][] table = {{"X","-","O"},{"-","O","X"},{"O","-","X"}};
        String yourTurn = "O";
        assertEquals(false, OXProgram.checkwin (table,yourTurn));
                }
    
  
    
    
}

